# Game List

## About The Project

This application shows a list of games from the [Gamerpower API](https://www.gamerpower.com/api-read), as well as detailed information about them.


### Built With

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.4.
[![Angular][Angular.io]][Angular-url]


## Getting Started

### Prerequisites

Before running the application, you will need to install [Node.js and NPM package manager](https://nodejs.org/en/download).

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/t.podkur17/game-list.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Run a dev server
   ```js
   ng serve
   ```
4. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
