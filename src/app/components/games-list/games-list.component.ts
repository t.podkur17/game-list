import { Component, OnInit } from '@angular/core';
import { GamesService } from '../../services/games.service';
import { IGame } from '../../models/game';
import { NavigationPath } from '@app/core/navigation/common';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss'],
})
export class GamesListComponent implements OnInit {
  NavigationPath = NavigationPath;
  loading: boolean = false;
  games: IGame[] | undefined;

  constructor(public gamesService: GamesService) {}

  ngOnInit(): void {
    this.loading = true;
    this.gamesService.getAll().subscribe((res: IGame[]) => {
      this.loading = false;
      this.games = res;
    });
  }
}
