export enum NavigationPath {
  Home = '',
  GameDetails = 'games/:id',
}
