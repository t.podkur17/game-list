import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GamesService } from '../../services/games.service';
import { IGame } from '../../models/game';

@Component({
  selector: 'app-game-details-page',
  templateUrl: './game-details-page.component.html',
  styleUrls: ['./game-details-page.component.scss'],
})
export class GameDetailsPageComponent implements OnInit {
  loading: boolean = false;
  game: IGame | undefined;
  constructor(private readonly activatedRoute: ActivatedRoute, public gamesService: GamesService) {}

  ngOnInit(): void {
    this.loading = true;
    this.gamesService.getGameById(this.activatedRoute.snapshot.params['id']).subscribe((res: IGame) => {
      this.loading = false;
      this.game = res;
    });
  }

  breakInstructions(instructions: string): string[] {
    return instructions.split('\n');
  }
}
